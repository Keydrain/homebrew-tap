class ConfluentPlatform < Formula
  desc "Confluent Kafka CLI Tools"
  homepage "https://www.confluent.io/product/confluent-platform/"
  url "https://packages.confluent.io/archive/5.5/confluent-5.5.1-2.12.tar.gz"
  version "5.5.1"
  sha256 "60efb18fb5768d05bbe7d0194fc6b61018ea3bd562d8829f9b14d9b5dd35f790"

  license "Apache-2.0"

  livecheck do
    url "https://docs.confluent.io/current/release-notes/changelog.html"
    regex(/>Version (\d+(?:\.\d+)+)</i)
  end

  depends_on "openjdk@8"

  conflicts_with "kafka", because: "kafka also ships with identically named Kafka related executables"

  def install
    libexec.install %w[bin etc libexec share]
    rm_rf libexec/"bin/windows"

    # Delete some lines to avoid the error "../java: No such file or directory"
    inreplace libexec/"bin/confluent-hub", "[ -L /usr/local/bin/confluent-hub ]", "false"

    bin.write_exec_script Dir["#{libexec}/bin/*"]
  end

  test do
    assert_match version.to_s, shell_output("#{bin}/kafka-broker-api-versions --version")
    assert_match /unable to load config/, shell_output("#{bin}/confluent 2>&1", 1)
    assert_match /usage: confluent-hub/, shell_output("#{bin}/confluent-hub help")
  end
end
