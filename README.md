# Keydrain's Tap

## Disclaimer

~~I don't know what I'm doing.~~
I _might_ have an idea what I'm doing.

This is still a work in progress and any suggestions or improvements are welcome.

If I've packaged up your application and you would prefer that I didn't, let me know and it'll be removed.

## How do I install these formulae?

### Basic install

```
brew tap keydrain/tap git@gitlab.com:Keydrain/homebrew-tap.git
brew install keydrain/tap/confluent-platform
brew install cask keydrain/tap/sdm
```

### [nix-darwin](https://github.com/LnL7/nix-darwin)

Add the following to your darwin-configuration.nix:

```
homebrew = {
  enable =true;
  taps = [
    ''keydrain/tap", "git@gitlab.com:Keydrain/homebrew-tap.git''
  ];
  brews = [
    "keydrain/tap/confluent-platform"
  ];
  casks = [
    "keydrain/tap/sdm"
  ];
}
```

## Documentation

`brew help`, `man brew` or check [Homebrew's documentation](https://docs.brew.sh).
