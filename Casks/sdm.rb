cask 'sdm' do
  version '15.27.0'
  sha256 '469de4f3e41be7fc2448284ab3066eade4452b3967c6b7b8ba1ff7d35ffa04b4'

  # sdm-releases-production.s3.amazonaws.com was verified as official when first introduced to the cask
  url "https://sdm-releases-production.s3.amazonaws.com/builds/sdm-gui/#{version}/darwin-full/151380029ABCD56896525527125D712D896AAB4D/SDM-#{version}.dmg"
  name 'StrongDM'
  homepage 'https://strongdm.com/'

  app 'SDM.app'
end
